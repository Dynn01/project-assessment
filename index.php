
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">  
  <title>Home</title>
</head>
<body>
  <div class="header">
    <div class="vw-100">
      <div id="navbar">
        <nav class="navbar bg-dark mb-0">
          <div class="col-2 col-sm-1">  
            <img src="img/icon.png" alt="icon" width="150px">
          </div> 
          <div class="col-10">  
            <h1 id="text" class="text-white">My Soccer Team</h1>
          </div>
        </nav>
      </div>
    </div>
  </div>
  <div  id="view" class="container-fluid vw-100">
    <div class="row">
      <div class="col-12">
        <div class="col-12 d-flex">
          <div class="d-flex flex-column mx-auto mt-5 mb-5">
            <h1 class="display-3" id="font">Wellcome !</h1>
          </div>
        </div>
        <div class="col-12 d-flex">
          <div class="d-flex flex-column mx-auto">
            <a href="list_team.php">
              <img class="border rounded-circle" id="viewTeam" src="img/btn-icon.png" alt="team" width="180" height="180">
            </a>
          </div>
        </div>
        <div class="col-12">
          <div class="d-flex">
            <div class="d-flex flex-column mx-auto mt-4 mb-5">
              <h1 id="font">click the button above to see My Team</h1>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mb-0">  
      <div id="footer" class="row">
        <div class="col-12 bg-dark">
          <div class="d-flex mt-3 mb-4">
            <div class="d-flex flex-column mx-auto">
              <small class="text-light font-weight-lighter font-italic">khoirudin copyright&copy;2020 </small>
            </div>
          </div>
        </div>
      </div>
  </div>  
    <!-- Optional JavaScript; choose one of the two! -->
    
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    
    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
  </html>