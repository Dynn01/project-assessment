<?php

include 'inc/connection.php';
include "inc/functions.php";


if (isset($_GET['delete_id'])) {
  deletePlayer(base64_decode($_GET['delete_id']));
}else {
  if (isset($_POST['action'])) {
    if (preg_match('/^[A-Z a-z]*$/',$_POST['name'])) {
      addPlayer($_POST['name'],$_POST['position'],$_POST['number']);
    }else {
      $error = "Please input a valid name";
    }
  }
}

if (isset($_POST['edit'])) {
  updatePlayer($_POST['position'],$_POST['number'],$_POST['id']);
}

$temp=[];
if (isset($_POST['btn-search'])) {
  $temp = search($_POST['search']);
  if(!empty($temp)){
    $data_player = $temp;
  }else {
    $msg=$_POST['search']." Not Found";
  }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>List Team</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">  
  <style>
    body {
    background-image: url(../img/Soccer-Rivals-Windows-10-wallpaper.jpg);
    background-repeat: no-repeat;
    background-position: top;
    }
   
  </style>
</head>
<body>
  <div class="content">
    <div class="header">
      <div id="navbar">
        <nav class="navbar bg-dark mb-0">
          <div class="col-2">  
            <img src="img/icon.png" alt="icon" width="150px">
          </div> 
          <div class="col-8">  
            <h1 id="text" class="text-white">List of My Soccer Team</h1>
            <form class="form-inline" action="list_team.php" method="POST">
              <input class="form-control mr-sm-2" name="search" type="search" placeholder="Search" aria-label="Search Player">
              <button class="btn btn-outline-success my-2 my-sm-0" name="btn-search" type="submit"><i class="fas fa-search"></i></button>
            </form>
          </div>
          <div class="col-2">
            <a href="index.php" class="nav-link nav-item"><i class="fas fa-home float-right ml-3" style="color: limegreen; font-size: 50px;"></i></a><i type="button" class="fas fa-user-plus float-right" data-toggle="modal" data-target="#modal" style="color: limegreen; font-size: 45px; margin-bottom: 20px"></i>
          </div>  
        </nav>
      </div>  
    </div>
  </div>
  <div class="container-fluid mt-5">
    <section id="home">
      <div class="container">
        <div class="row">  
          <div class="col-md-12 col-sm-12">
            <?php 
            if (isset($msg)) {
              echo '<div class="alert alert-success" role="alert">';
                echo $msg;
                echo '</div>';
              }elseif (isset($error)) { 
                echo '<div class="alert alert-danger" role="alert">';
                  echo $error;
                  echo  '</div>';
                }
                ?>
                
              </div>
              
              <?php foreach($data_player as $value): ?>
              <div  id="card" class="card text-center border 3px border-dark mb-2 col-md-6 col-sm-12">
                <div class="card-header">
                  <img class="float-left" src="img/dream_team.png" alt="dream_team" width="80px">
                  <ul class="nav nav-pills card-header-pills float-right"> 
                    <li class="nav-item">
                      <a href="edit.php?edit='<?php echo base64_encode($value['id']); ?>'"><i class="fas fa-user-edit btn btn-success" style="font-size: 20px;"></i></a>
                    </li>
                    <li class="nav-item">
                      <a href="list_team.php?delete_id='<?php echo base64_encode($value['id']); ?>'" onclick="return confirm('Are You Sure to Delete this Player ?')"><i class="fas fa-user-times btn btn-danger" style="font-size: 20px;"></i></a>
                    </li>
                  </ul>
                </div>
                <div class="card-body">
                  <table class="table table-hover">
                    <thead class="thead-light">
                      <tr id="text-card">
                        <th scope="col"></th>
                        <th scope="col" style="font-size: 20px;">Name</th>
                        <th scope="col" style="font-size: 20px;">Position</th>
                        <th scope="col" style="font-size: 20px;">Number</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row"><img class="border rounded-circle" src="img/football-player.png" alt="football-player" width="30px"></th>
                        <td class="font-weight-bold"><?php echo $value['name']; ?></td>
                        <td class="font-weight-bold"><?php echo $value['position']; ?></td>
                        <td class="font-weight-bold"><?php echo $value['back_number']; ?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <?php endforeach; ?>                     
            </div>
          </div>
        </section>           
      </div>
    </div>
  </div>  
      
      <div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h3 id="text" class="modal-title">Add Player</h3>
            </div>
            <form action="list_team.php" method="POST">
              <div class="modal-body">
                <ul class="mr-5">
                  <li class="list-group">
                    <div class="form-group">
                      <label id="text-modal" for="inputName">Name</label>
                      <input type="text" name="name" class="form-control" id="inputName" required>
                    </div>
                  </li>
                  <li class="list-group">
                    <div class="form-group">
                      <label id="text-modal" for="inputPosition">Position</label>
                      <select id="inputPosition" name="position" class="form-control">
                        <option value="Striker">Striker</option>
                        <option value="Wide Midfield">Wide Midfield</option>
                        <option value="Sweeper">Sweeper</option>
                        <option value="Full-Backs">Full-Backs</option>
                        <option value="Centre-Backs">Centre-Backs</option>
                        <option value="GoalKeeper">GoalKeeper</option>
                      </select>
                    </div>
                  </li>
                  <li class="list-group">
                    <div class="form-group">
                      <label id="text-modal" for="inputNumber">Number</label>
                      <input type="number" name="number" class="form-control" id="inputNumber" required>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Cancel</button>
                <button type="submit" name="action" id="send" class="btn btn-success">Save</button>
              </div>
            </form>
          </div>
        </div>    
    
      
      <!-- Optional JavaScript; choose one of the two! -->
      
      <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
      
      <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
      -->      
      
      
</body>
</html>