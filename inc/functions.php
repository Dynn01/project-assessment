<?php


function addPlayer($name,$position,$back_number) {
   
    include 'connection.php';

        $sql = 'INSERT INTO player(name, position, back_number) VALUES(?, ?, ?)';

    try {
        $result = $db->prepare($sql);
        $result->bindValue(1, $name, PDO::PARAM_STR);
        $result->bindValue(2, $position, PDO::PARAM_STR);
        $result->bindValue(3, $back_number, PDO::PARAM_INT);

        $result->execute();
        header('location: list_team.php');
    } catch (Exception $e) {
        echo "Error!: " . $e->getMessage() . "<br />";
        return false;
    }
    return true;
}

function deletePlayer($id) {
    include 'connection.php';

    $sql = 'DELETE FROM player WHERE id= ?';

    try {
        $result = $db->prepare($sql);
        $result->bindValue(1, $id, PDO::PARAM_STR);

        $result->execute();
        header('location: list_team.php');
    } catch (Exception $e) {
        echo "Error!: " . $e->getMessage() . "<br />";
        return false;
    }
    return true;
}


function updatePlayer($position, $number, $id=null)
{
    include 'connection.php';

    $sql = 'UPDATE player SET position = ?, back_number = ? WHERE id = ?';

    try {
        $result = $db->prepare($sql);
        $result->bindValue(1, $position, PDO::PARAM_STR);
        $result->bindValue(2, $number, PDO::PARAM_INT);
        $result->bindValue(3, $id, PDO::PARAM_INT);

        $result->execute();
        header('location: list_team.php');
    } catch (Exception $e) {
        echo "Error!: " . $e->getMessage() . "<br />";
        return false;
    }
    return true;    
}

function loopSearch($query)
{
    include 'connection.php';

    $result = $db->query($query);
    $rows=[];
    while($row = $result->fetch(PDO::FETCH_ASSOC))
    {
        $rows[]=$row;
    }
    return $rows;
}

function search($keyword)
{
    $sql = "SELECT * FROM player WHERE name LIKE '%$keyword%' OR position LIKE '%$keyword%' OR back_number LIKE '%$keyword%'";

    return loopSearch($sql);

}