<?php 

include 'inc/connection.php';

$editData = $db->query("SELECT * FROM player WHERE id=".base64_decode($_GET['edit']));
$edit = $editData->fetchAll();

$position =['Striker','Wide Midfield','Sweeper','Full-Backs','Centre-Backs'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <style>
        body {
            background-image: url(img/bg_neymar.jpg);
            background-repeat: no-repeat;
            background-size: cover;
            bottom: 0;
            right: 0;
            
        }
        #loading {
            width: 100%;
            height: 100%;
            top: 0px;
            left: 0px;
            position: fixed;
            display: block;
            opacity: 0.4;
            background-color: white;
            z-index: 99;
            text-align: center;
        }
        
        #loading-image {
            position: absolute;
            top: 200px;
            left: auto;
            z-index: 100;
        }
        
    </style>
    
</head>
<body>
    <div class="container">
        <div class="row">
            <form action="list_team.php" method="POST" class="border border-primary rounded p-5 col-4">
                <h1 id="text-header" class="my-4">Edit Player</h1> 
                <div class="form-group">
                    <label id="text-edit" for="inputPosition">Position</label>
                    <select id="inputPosition" name="position" class="form-control">
                        <?php foreach ($position as $key): ?> 
                        <option <?php if($edit[0]['position'] == $key){ echo "selected"; } ?> value="<?php echo $key; ?>"><?php echo $key; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group mx-auto">
                    <label id="text-edit" for="inputNumber">Number</label>
                    <input type="number" class="form-control" name="number" value="<?php echo $edit[0]['back_number']; ?>">
                </div>
                <input type="hidden" name="id" value="<?php echo $edit[0]['id']; ?>">
                <button type="submit" id="button" name="edit" class="btn btn-primary">save</button>
            </form>
        </div> 
    </div>
    <div class="col-12 d-flex">
        <div id="loading">
            <img id="loading-image" src="http://cdn.nirmaltv.com/images/generatorphp-thumb.gif" alt="Loading">
        </div>
    </div>
    <!-- Optional JavaScript; choose one of the two! -->
    
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
    
    <script>
        $(document).ready(function() {
            $('#loading').hide();
            $('#button').click(function() {
                $('#loading').show();
            });
        });
    </script>
    
</body>
</html>